# Docker-compose for development

Set up a fast dev environment (`node-front-starter` and `node-back-starter`) using docker.

1.  `sh build.sh`

- (linux) just run it like any shell script
- (windows) you can use Git Bash in the project directory to run it or use WSL2

2.  `docker-compose up`

## Common issues fixes

- Windows user ? You need to set the line endings to LF
- Are local ports are available ?
- Is docker running ?
- Permission error ? `chmod +x build.sh`