#!/bin/sh

git submodule update --init
git submodule foreach git checkout develop
git submodule foreach npm i

# .env

cp ./node-front-starter/.env.dist ./node-front-starter/.env.local

cp ./node-back-starter/.env.dist ./node-back-starter/.env